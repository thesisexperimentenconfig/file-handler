var Zip = require('node-zip');
var fs = require('fs');
var path = require('path');

module.exports = {
	
	internalName : 'representation.json',
	
	/**
	 * Zips a string representation of the project into one a file.
	 * 
	 * @param  {[type]} fileStrings [description]
	 * @param  {[type]} filename    [description]
	 * @return {[type]}             [description]
	 */
	makeExperimentFile: function(stringRepresentation, filename){
		var zipper = new Zip();
		zipper.file(this.internalName, stringRepresentation);
		var data = zipper.generate({base64:false,compression:'DEFLATE'});
		filename = path.normalize(filename);
		fs.writeFileSync(filename, data, 'binary');
	},

	/**
	 * Opens a file and returns the string representation
	 * @param  {string} filename where the file is located
	 * @return {string}          The string representation of the file.
	 */
	openExperimentFile: function(filename){
		filename = path.normalize(filename);
		var data = fs.readFileSync(filename);
		var zipper = new Zip(data,{base64: false, checkCRC32: true});
		
		return zipper.files[this.internalName]._data;
	}
	
}