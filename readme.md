# Usage 
`makeExperimentFile(representation: String, filename: String)`: creates a file from the `representation` and stores it at `filename`.

`openExperimentFile(filename: String): String`  opens the file and returns the data as a string representation.

# Tests
After Downloading
    npm install
    npm install -g istanbul

run the command
    npm test
	
in terminal.