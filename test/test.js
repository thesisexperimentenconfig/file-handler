var fileHandler = require('../index.js');
var fs = require('fs');

var should = require('chai').should();
var expect = require('chai').expect;

describe('File handling test', function(){
	var json = {
			test: "test"
	};
		
	it('test write to file', function(done){
		

		var path = __dirname + '/file2.simp';
		
		// create the file
		fileHandler.makeExperimentFile(JSON.stringify(json), path);
		
		
		// check if the file exists
		fs.stat(path, function(err, stat) {
			// if there is an error, the tests fails
			if(err){
				return done(err);
			}
			
			fs.unlinkSync(path);
			done(err);
		});
		
	});
	
	it("Test open file", function(){
		var path = __dirname + '/file.simp';
		var data = fileHandler.openExperimentFile(path);
		
		data.should.eql(JSON.stringify(json));
	})
});